from controller import app
from controller.frontend import *
from controller.user import *

app.register_blueprint(frontend)
app.register_blueprint(user, url_prefix = '/user')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9000, debug = True)
