from flask import Blueprint

__all__ = [
    'frontend',
    'index'
]

frontend = Blueprint('frontend', __name__)
