from controller.user import user

@user.route('/cp/')
def control_panel():
    return "This is the User Control Panel"


@user.route('/cp/change_password/')
def change_password():
    return "Here users can change their passwords."


@user.route('/cp/messages/')
def messages():
    return "Here users can view their private messages."
