from flask import Blueprint

__all__ = [
    'user',
    'control_panel',
    'profile'
]

user = Blueprint('user', __name__)
