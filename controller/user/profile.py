from controller.user import user

@user.route('/profile/')
def profile_list():
    return "This page would list all the user profiles."


@user.route('/profile/<int:id>/')
def profile(id = None):
    if not id:
        return "No user profile id given."

    return "This is the profile for the user id " + str(id)
